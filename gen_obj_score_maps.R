

####****************************************************************************
####
#### Generate objective score maps by intervention ####
####
####****************************************************************************

# This is the part that actually calls InVEST models, everything else is just
# R based

for(expi in runs_to_do){

  for (i in 1:length(interv_list)){

    active_suffix <- paste(place, names(interv_list)[[i]], mvti, expi, sep="_")

    # index specifying resolution coarsening
    res_active_suffix <-  paste0(1, "_", active_suffix)

    #SDR:
    if(do_SDR){


      # Build the mapping of what model arguments should be changed to the
      # values provided in the experimental design: First col is model arg name,
      # 2nd is header name of corresponding column in exp design file.

      #Define what args to grab:
      sdr_args_to_set <- as.data.frame(rbind(
        c("lulc_path",              paste(getwd(), "/", lulc_names[i], sep="")),
        c("biophysical_table_path", mv_exp_design$sdr_biophysical_table_path[expi]),
        c("k_param",                as.character(mv_exp_design$sdr_k_param[expi])),
        #c("erosivity_path",        mv_exp_design$erosivity[expi]),
        c("results_suffix",         NA)),
        stringsAsFactors=FALSE)

      #Old results suffix, pre active suffix and doesn't have place:
      #c("results_suffix",         paste(names(interv_list)[i],"_", mvti, "_", expi, sep=""))),

      names(sdr_args_to_set) <- c("to_set", "set_to")


      args_to_set <- sdr_args_to_set
      args_to_set[args_to_set$to_set=="results_suffix", "set_to"] <- res_active_suffix
      # 1 is aggregation factor -- running in native resolution

      outfile <- paste(script_dir, "sdr_call_", res_active_suffix, ".py", sep="")

      adj_py_model_script(template_script = paste0(sdr_template_script_root, place, ".py"),
                          args_to_set = args_to_set,
                          outfile = outfile)


      cat("Calling SDR, ESM table row ", expi, ", intervention ",
          names(interv_list)[[i]], " (", i, " of ", length(interv_list), ")\n", sep="")

      #Run model!
      system(paste0("python ", outfile))

      #Delete not-key files to save space during batching:
      #First find workspace: (a little cumbersome...)
      script_lines <- readLines(outfile)
      arg_ind <- grep(paste0("workspace_dir':"), script_lines)
      start_char <- regexpr("': u'", script_lines[arg_ind]) + 5
      end_char <- regexpr("',", script_lines[arg_ind]) - 1
      active_model_workspace <- substr(script_lines[arg_ind], start_char, end_char)

      delete_files(files_csv="sdr_files_to_delete.csv", suffix=res_active_suffix,
                   model_workspace=active_model_workspace)

# Next three lines restart the for loop in case you need to do aggregation after
# you've already done the model runs
#  for (expi in 1:17){
#    for (i in 1:length(interv_list)){ # TEMP TEMP TEMP
#      active_suffix <- paste(place, names(interv_list)[[i]], mvti, expi, sep="_") # TEMP TEMP TEMP
#      res_active_suffix <-  paste0(1, "_", active_suffix) #specifying resolution coarsening # TEMP TEMP TEMP

      for (agg_fact in agg_factors){

        if(agg_fact==1){
          # do nothing
        } else {
          rtgf <- runs_to_grab_full
          rtgfi <- which(rtgf$obj=="sed")
          to_coarsen <- raster(paste0(rtgf[rtgfi, "outfile_path"], "/", rtgf[rtgfi, "outfile_root"], res_active_suffix, ".tif"))
          aggregate(to_coarsen, fact=agg_fact, fun=sum, expand=TRUE, na.rm=TRUE,
                    filename=paste0(rtgf[rtgfi, "outfile_path"], "/", rtgf[rtgfi, "outfile_root"], agg_fact, "_", active_suffix, ".tif"),
                    overwrite=TRUE)

          rtgfi <- which(rtgf$obj=="soil")
          to_coarsen <- raster(paste0(rtgf[rtgfi, "outfile_path"], "/", rtgf[rtgfi, "outfile_root"], res_active_suffix, ".tif"))
          aggregate(to_coarsen, fact=agg_fact, fun=sum, expand=TRUE, na.rm=TRUE,
                    filename=paste0(rtgf[rtgfi, "outfile_path"], "/", rtgf[rtgfi, "outfile_root"], agg_fact, "_", active_suffix, ".tif"),
                    overwrite=TRUE)

        }
      }
    }
#  }

    #AWY:
    if(do_AWY){

      #Annual Water Yield args to set
      awy_args_to_set <- as.data.frame(rbind(
        c("lulc_uri",              paste(getwd(), "/", lulc_names[i], sep="")),
        c("biophysical_table_uri", mv_exp_design$awy_biophysical_table_uri[expi]),
        c("seasonality_constant",  as.character(mv_exp_design$awy_seasonality_constant[expi])),
        #  json_data[["precipitation_uri"]] <- mv_exp_design$precip[expi]
        #  json_data[["eto_uri"]] <- mv_exp_design$PET[expi]
        c("results_suffix",         NA)),
        stringsAsFactors=FALSE)

      names(awy_args_to_set) <- c("to_set", "set_to")

      args_to_set <- awy_args_to_set
      args_to_set[args_to_set$to_set=="results_suffix", "set_to"] <- res_active_suffix

      outfile <- paste(script_dir, "awy_call_", res_active_suffix, ".py", sep="")

      adj_py_model_script(template_script = paste0(awy_template_script_root, place, ".py"),
                          args_to_set = args_to_set,
                          outfile = outfile)

      #Lame workaround to modify capitalization of logicals:
      #NOTE: Depending on the version written out, may or may not need to adjust
      #to match a possible space after the colon:
      #towrite <- gsub(":false", ":False", towrite)

      cat("Calling AWY, table row ", expi, " intervention ", names(interv_list)[[i]], "\n")

      #Run model!
      system(paste0("python ", outfile))

      #Delete not-key files to save space during batching:
      #First find workspace: a little cumbersome, but at least avoids hardcoding
      script_lines <- readLines(outfile)
      arg_ind <- grep(paste0("workspace_dir':"), script_lines)
      start_char <- regexpr("': u'", script_lines[arg_ind]) + 5
      end_char <- regexpr("',", script_lines[arg_ind]) - 1
      active_model_workspace <- substr(script_lines[arg_ind], start_char, end_char)

      delete_files(files_csv="awy_files_to_delete.csv", suffix=res_active_suffix,
                   model_workspace=active_model_workspace)

# Next three lines restart the for loop in case you need to do aggregation after
# you've already done the model runs
#    for (i in 1:length(interv_list)){ # TEMP TEMP TEMP
#      active_suffix <- paste(place, names(interv_list)[[i]], mvti, expi, sep="_") # TEMP TEMP TEMP
#      res_active_suffix <-  paste0(1, "_", active_suffix) #specifying resolution coarsening # TEMP TEMP TEMP

      for (agg_fact in agg_factors){

        if(agg_fact==1){
          # do nothing
        } else {
          rtgf <- runs_to_grab_full
          rtgfi <- which(rtgf$obj=="awy")
          to_coarsen <- raster(paste0(rtgf[rtgfi, "outfile_path"], "/", rtgf[rtgfi, "outfile_root"], res_active_suffix, ".tif"))
          aggregate(to_coarsen, fact=agg_fact, fun=sum, expand=TRUE, na.rm=TRUE,
                    filename=paste0(rtgf[rtgfi, "outfile_path"], "/", rtgf[rtgfi, "outfile_root"], agg_fact, "_", active_suffix, ".tif"),
                    overwrite=TRUE)
        }
      }

    }

    #SWY:

    if(do_SWY){ #NOT YET FINALIZED TO MATCH SWY INPUTS

      json_data <- fromJSON(paste(readLines(swy_json_file), collapse=""))

      json_data[["lulc_raster_path"]] <- lulc_names[i]
      json_data[["biophysical_table_path"]] <- mv_exp_design$awy_biophysical_table_uri[expi]
      json_data[["results_suffix"]] <- paste(names(interv_list)[i], "_", mvti, "_", expi, sep="")

      # json_data ref correct, but not yet corresponding value in mv_exp_design:
      #      json_data[["precip_dir"]] <-### mv_exp_design$precip[expi]
      #      json_data[["eto_dir"]] <- ###mv_exp_design$PET[expi]

      towrite <- toJSON(json_data)
      #Lame workaround to modify capitalization of logicals:
      #NOTE: Depending on the version written out, may or may not need to adjust
      #to match a possible space after the colon:
      towrite <- gsub(":false", ":False", towrite)
      towrite <- gsub(": false", ":False", towrite)

      fileConn<-file("swy_temp_args.json")
      writeLines(towrite, fileConn)
      close(fileConn)

      fileConn<-file(paste(json_dir, "swy_args_", json_data[["results_suffix"]], ".json", sep=""))
      writeLines(towrite, fileConn)
      close(fileConn)

      cat("Calling SWY, table row ", expi, " intervention ", names(interv_list)[[i]], "\n")

      system("python swy_test_default_data_argsfromfile.py")

      system("python swy_test_Gura_argsfromfile.py")
      #system("python swy_test_Sagana_argsfromfile.py")

    }

  }

  gc()

}

