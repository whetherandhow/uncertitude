---
title: "Pixel-level multi-objective optimization of ecosystem services with R"
author: "Benjamin P. Bryant"
date: "`r Sys.Date()`"
output: rmarkdown::html_vignette
vignette: >
  %\VignetteIndexEntry{Vignette Title}
  %\VignetteEngine{knitr::rmarkdown}
  %\VignetteEncoding{UTF-8}
---


//---
//title: "R-based pixel-level marginal value-based multi-objective optimization with InVEST and hyphens"
//author: "Benjamin P. Bryant"
//date: "`r format(Sys.time(), '%d %B, %Y')`"
//output: 
//  html_document:
//    toc: true
//    toc_depth: 4
//---



[Incomplete DRAFT DRAFT DRAFT -- (as will soon become obvious if reading on!)]

As implemented here, the spatial optimization consists of a non-trivial series of steps and intermediate products. The basic workflow to generate a single trade-off frontier, can be thought of as based on several sub-workflows, which can (in most cases) be treated as distinct:


## Phase 1: Estimate potential changes for all relevant pixels and all relevant interventions. 

This step involves generating numeric estimates (at each pixel) for how ecosystem service levels would change on the landscape if certain interventions were implemented. It is done through the following steps:

1. Generate static marginal value maps for each intervention by running each ecosystem service model with each intervention implemented wherever it is feasible to do so, and differencing with respect to ES provisioning levels on the default “base” landcover.

#

2. Generate a set of weight combinations, and specify cost for each intervention and total budget.
3. For each weight combinations:
    a. Create weighted objective scores for each pixel, for each intervention.
    b. Create weighted cost-effectiveness scores for each pixel, for each intervention, by dividing by the cost.
4. Sort at each pixel to identify the intervention with the highest weighted cost-effectiveness score for each pixel.

5. Use a greedy search to select the highest cost-effectiveness pixels until the budget constraint can no longer be satisfied. The set of chosen pixels defines a portfolio.
6. Extract the base value and change in each objective score for each pixel by indexing the portfolio against the static marginal value maps. This defines the objective score map and marginal value objective score maps.
7. Sum pixel-level scores to identify aggregate landscape-wide objective score values for a particular run.

8. Repeat for each weight combination to complete a frontier.

To generate many portfolios, the above steps are wrapped, by:
Specifying an experimental design for ES model inputs.
Specifying an experimental design for optimization parameters (budget, other constraints).
Generating unique index encodings for groupings that maybe relevant for visualization or analysis (eg, indexing tradeoff curves, or objective weight combinations).
Running the frontier-generating procedure defined above for each “row” of the full experimental design.


Vignettes are long form documentation commonly included in packages. Because they are part of the distribution of the package, they need to be as compact as possible. The `html_vignette` output type provides a custom style sheet (and tweaks some options) to ensure that the resulting html is as small as possible. The `html_vignette` format:

- Never uses retina figures
- Has a smaller default figure size
- Uses a custom CSS stylesheet instead of the default Twitter Bootstrap style

## Vignette Info

Note the various macros within the `vignette` section of the metadata block above. These are required in order to instruct R how to build the vignette. Note that you should change the `title` field and the `\VignetteIndexEntry` to match the title of your vignette.

## Styles

The `html_vignette` template includes a basic CSS theme. To override this theme you can specify your own CSS in the document metadata as follows:

    output: 
      rmarkdown::html_vignette:
        css: mystyles.css

## Figures

The figure sizes have been customised so that you can easily put two images side-by-side. 

```{r, fig.show='hold'}
plot(1:10)
plot(10:1)
```

You can enable figure captions by `fig_caption: yes` in YAML:

    output:
      rmarkdown::html_vignette:
        fig_caption: yes

Then you can use the chunk option `fig.cap = "Your figure caption."` in **knitr**.

## More Examples

You can write math expressions, e.g. $Y = X\beta + \epsilon$, footnotes^[A footnote here.], and tables, e.g. using `knitr::kable()`.

```{r, echo=FALSE, results='asis'}
knitr::kable(head(mtcars, 10))
```

Also a quote using `>`:

> "He who gives up [code] safety for [code] speed deserves neither."
([via](https://twitter.com/hadleywickham/status/504368538874703872))



Break this into the conceptual setup needed to do a single optimization, versus broader infrastructure required or batching?

# Elements of decision problem

## Objectives, vs models

For now, assumiming InVEST model is objective

## Decision units, versus model units

pixels? Polygons? Aggregated?

## Constraints

For now just one type. 

# Somewhere: directories and input files

Things you need to specify somewhere and somehow:

* Working directory for each model [model run outputs will go here]
* Template script for each model with default parameters and input paths
* One or more biophysical tables [for each model, or one]
* Experimental design files [separate for each stage?]
* One or more output directories for outputs of the optimazation process
    * Marginal value tables and rasters
    * R-native solution objects and rasters
    * Real and/or imputed values objective score maps
    * summary objects

Note that if you are working in shared or cloud-sync'd directories it might be a good idea to avoid writing out ES model runs, and possibly optimization solutions, to those directories in order to avoid lots of needless file uploads and downloads. 

# Decide on models for entire process

# Specifying interventions and allowable LU's for each intervention.

Each intervention specified by LULC [possibly unique]

InVEST models typically require categorical land cover maps to run. For most land based models, intervention on a pixel is represented by changing to a different LULC category with appropriately different parameter values. Furthermore, not all interventions can be applied on any LULC. 

In order to generate more accurate static marginal value maps and to enforce a feasible optimization solution, we need to specify these relationships. These are done by 

XXX --- in a CSV file, that stores them as a list. 

## Create biophysical table 

For now, assuming one biophysical table that serves all models, but this is not conceptually necessary.

# Step 1: Get each model to run in a not-obviously wrong way

In the area you want.



# STEPS: Batching the models

## Tailoring helper scripts

### Create a template script for each model

This script should have the arguments you want as the default EVEN IF they are not modified. Even if you WILL modify them, it still needs to be created, because the code (as of Feb ish) works by adjusting arguments defined in a script, rather than just _creating_ a script from scratch. 

To be explicit, your model should run at the SYSTEM command line with:

python whateverscriptname.py 

And also at the R command line with:

system("python whateverscriptname.py")

(If the former works but not the latter, there is a problem in the R interface, not the script.

For the Tana application, the [to be distributed] file are:

awy_template.py
sdr_template.py
swy_template.py

It is strongly suggested, but not technically necessary, that the template files be constructed to run of the region of interest. It will still work with a template file from another area so long as the actual experimental design file (discussed below) ensures harmony of inputs. 


### Specify files you are pretty sure you don't need

By default, each InVEST model produces quite a bit of intermediate and/or auxiliary output. These outputs can be useful for diagnosing model performance, and in some cases these other outputs might more closely correlate to an objective than the "main" output of the model (eg, concern about soil erosion across the landscape might be better assessed by looking at the USLE output in SDR, rather than the [SED_]_export output, which is focused on stream impacts. 

In general, doing large numbers of batch runs is not feasible because hard drive space may get eaten up, so we would like to delete files that are large and unlikely to be used. This package contains a helper function to make it (relatively) easy to specify what files to delete. 

From Perrine:
All the monthly QFs can be deleted (in intermediate folder).
Also Vri and CN (in main folder), and very likely B_sum and L_sum_avail.

```{r}
summary(cars)
```

You can also embed plots, for example:

```{r, echo=FALSE}
plot(cars)
```

Note that the `echo = FALSE` parameter was added to the code chunk to prevent printing of the R code that generated the plot.

## Building an experimental design

The experimental design is a function of the problem architecture, which ideally should not needless run models or regenerate data structures. For example, marginal values are different under different biophysical parameter combinations, but (not accounting for spatial interdependency) remain the same independent of objective weights. 

Therefore we attempt in our experimental design to build up these issues independently, and combine them "smartly." 

Ultimately we have:

Set of marginal value tables, which are affected by biophysical table values and other ES model inputs -- both scalar and raster. 

We then have optimization parameters that are unchanged by model behavior:

objective score weights (as relevant), intervention costs and budget constraints (or their generalized form), and constraints on pixel conversions.

NOTE NOTE: Need to check how "embedded" the representation of implementation constraints are between marginal values and not. 

We discuss this "elements" or "chunks" of the experimental design accordingly:

### Model or marginal-value based experimental variation

#### Biophysical tables

#### Scalar parameters to specific models

#### File (especially spatial) inputs to specific models

### Optimization problem experimental variation

#### Objective weights

#### Activity costs

#### Budget


## Generating marginal value tables

## Generating optimization solutions


# Own section or elsewhere: Key data structures


# Analyzingg output

## Plotting portfolios

## Assessing trade-offs

## Robustness, confidence and uncertainty



