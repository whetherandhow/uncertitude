
rm(list=ls())
library(raster)
#library(ggplot2) #TODO -- move to plotting script?
#library(rjson) # still necessary?
#library(cobs) #for 2-d frontier fitting

setwd("C:/Users/bpb/Dropbox/Tana Optim Set")

code_dir <- "C:/Users/bpb/Dropbox/uncertitude"

##******************************************************************************
##
####  Bring in and construct required workspace objects ####
##
##******************************************************************************

source(paste(code_dir, "define_run_set.R", sep="/"))
gc()