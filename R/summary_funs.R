
# Multiple summarizing functions that should get called from other scripts
# Note, moved many to "summary_funs_unvetted.R" in prep for R package, slowly
# bringing back into the 'official' set based on need and robustness.



####****************************************************************************
####
####  sum_stats  ####
####
####****************************************************************************

#' Pixel-level summary stats for ensemble of rasters
#'
#' @param x A vector over which to generate various summary statistics --
#'          assumed at the level of a pixel extracted from a raster stack.
#'
#' @return a vector with one entry per summary statisic, currently four. See
#'         Details.
#'
#' @details Note, this function is designed to be passed to the \code{calc}
#'          function in the package \code{raster}.
#'
#' @details A RasterBrick or RasterStack where each layer is any summary
#'          statistic you want. As of 0.0.0-9000, there are four:
#'       mode_raw: Mode across layers of a pixel, considering all pixels
#'       frac_agree_raw: Fraction of time the modal value is chosen
#'       mode_conditional: Mode conditional on an intervention being chosen
#'       frac_agree_conditional: Fraction of time particular intervention
#'       chosen, out of times an intervention was chosen.
#'
#' @export
#
# @examples
# rs <- some_raster_stack #TODO
# summary_layers <- calc(rs, fun=sum_stats)


sum_stats <- function(x) {#Need to add "no intervention" value as argument

  # NOTES:  May need to investigate how ties are handled for modal stats
  #         (currently might just take lowest value)
  #         Consider adding categorical variance index (eg, shannon diversity or
  #         "unlikability".

  #First, check for all NA's (these will exist for rectangular raster covering
  #a non-rectangular zone)

  if(all(is.na(x))){
    mode_raw <- NA
    frac_agree_raw <- NA
    mode_conditional <- NA
    frac_agree_conditional <- NA
  } else {

    ## Raw statistics: (Ie, does not condition on there being an intervention)

    ux <- unique(x)
    mode_raw <- ux[which.max(tabulate(match(x, ux)))]
    #    frac_agree_raw <- mean(x==mode_raw)
    if(!is.na(mode_raw)){
      frac_agree_raw <- sum(x==mode_raw, na.rm=TRUE)/length(x)
    } else {
      frac_agree_raw <- sum(is.na(x)/length(x))
    }

    ## Conditional statistics: (Make summaries only for those pixels where an
    ## intervention was chosen)

    #Specifiy the cell value that indicates there was no activity chosen:
    unchanged_code <- NA #DUMMY FOR NOW
    #TODO -- genericify as argument

    # Test whether there are any interventions at all. If not, assign NA's to
    # all the results.  If NA is change-code, shouldn't get inside first if,
    # but this allows different 'missing' and 'do-nothing' values.

    if(is.na(unchanged_code)){
      changed_vec <- !is.na(x)
    } else{
      changed_vec <- x!=unchanged_code
    }

    if (sum(changed_vec, na.rm=TRUE)==0){ #Note, this will make it conditional
      #on not being missing too. Still
      #thinking about whether that could be
      #a problem.

      # NOTE/TODO: Not sure when one would get here...

      mode_conditional <- NA
      frac_agree_conditional <- NA

    } else { # The pixel contains a mix of runs where an intervention was
      # chosen and some where it wasn't. Condition on an intervention being
      # chosen:

      x_conditional <- x[changed_vec]
      stopifnot(!any(is.na(x_conditional))) #shouldn't be any NA's

      ux_conditional <- unique(x_conditional)

      mode_conditional <- ux_conditional[which.max(tabulate(match(x_conditional,
                                                      ux_conditional)))]

      # TODO: Assignment old/unnecessary because always written over?
      # frac_agree_conditional <- mean(x_conditional==mode_conditional)

      if(!is.na(mode_conditional)){
        frac_agree_conditional <- sum(x==mode_conditional, na.rm=TRUE)/length(x_conditional)
      } else {
        # Shouldn't get here? Unless NA is special type of intervention?
        print("Wasn't expecting to get into this else statement.")
        browser()
        frac_agree_conditional <- sum(is.na(x_conditional)/length(x_conditional))
      }


    }

  }

  if(!is.na(mode_raw) & is.na(mode_conditional)){browser()}

  #Assemble into return vector:
  return(c(mode_raw = mode_raw,
           #mode_raw = as.integer(mode_raw),
           frac_agree_raw = frac_agree_raw,
           mode_conditional = mode_conditional,
           #mode_conditional = as.integer(mode_conditional),
           frac_agree_conditional = frac_agree_conditional)
  )


  #### Below here are some comments on earlier versions of this function which I
  # have not assessed are still relevant.

  #### Need to add:
  #raster value indicating no intervention should become argument to sum_stats
  #function, rather than hardcoded within it.

  #Add labeling of stats layers for ease of review.

  #### Might add:
  #Seems like maybe it would be useful to make the modal value layers
  #distinguish between pixels where NA was the modal value, but sometimes
  #other activities were selected, vs pixels where nothing was selected?

  #Handling for continuous variables, depending on whether making R complement
  #to Peter's code, or just making quick and dirty product for RIOS analysis.

  #May test whether possible to improve speed by
  #capitalizing on integer or categorical (factor) data types. May be more
  #trouble than it's worth though.

  #Test whether there are significant speed advantages to converting a
  #RasterStack to RasterBrick. If so, set as option with flag, filename, etc.

  #Test whether there are significant speed advantages to utilizing built in
  #raster::modal -- guessing there would be if unconditioned stats were the only
  #thing we cared about, but since lots of other custom calcs that still need to
  #be made, probaby not the case.

  #Test forcing memory-based processing vs chunking

}




####****************************************************************************
####
####  aggregate_rasts: Batch aggregate many rasters  ####
####
####****************************************************************************


# Depends: tools package (for file_ext())

#' Batch aggregate many rasters
#'
#' @description Convenience wrapper function to raster::aggregate, to perform
#' aggregation on a large number of rasters.
#'
#'
#' @param filenames Vector Of Raster Filenames: Character vector of original map file
#'  names.
#'
#' @param agg_fact integer or length 2 vector,equivalent to the "fact" argument
#' in raster::aggregate, see details.
#'
#' @param agg_fun modal for categorical, sum or mean for continuous
#  NEED TO CHECK can pass...
#'
#' @param outfile_type Character string, indicating what type of raster the
#' aggregated raster should be written as. Either "preserve" in which case it
#' attempts to extract the input raster type and use that, or a string
#' corresponding to an extension recognized by raster::writeRaster.
#'
#' @return None, used for side effect of writing to file.
#'
#' @details If you don't have your vector of filenames handy, consider using
#' \code{dir} (and analogs) plus and some regular expressions to build it.
#'
#' NOTE: Aggregation factor is defined by side-length, not area. The pixel side
#' length of new raster size is \code{agg_fact} times the side length of the
#' pixel in the original raster. For example, assuming square aggregation, an
#' agg_fact of 4 reduces pixel count by 16.
#'
#' The script will write out files of the same name with _[agg_fact] appended
#' before the file extension.
#'
#' Warning: Not tested on multi-band rasters, might break or only extract
#' first band.
#
#' @export
#
# @examples

aggregate_rasts <- function(filenames, agg_fact, agg_fun, outfile_type="preserve"){

  for (i in 1:length(mfns)){

    if(outfile_type=="preserve"){
      outfile_ext <- file_ext(filenames[i])
    }

    hi_res_rast <- raster(filenames[i])
    new_rast <- aggregate(hi_res_rast, fact=agg_fact, fun=agg_fun)
    writeRaster(new_rast, paste0(filenames[i], "_", agg_fact, ".", outfile_ext))

  }

  return(NULL)

  # Note: I think this can all be done technically in one line inside for loop
  # using the filename option, but whatever.

  # Accept alternate argument for output files
  # TODO: Accept ... to pass other args to "aggregate()" -- maybe even fact
  # and fun?
  # TODO/nicety: Abity to change outfile formate beyond just filename.
  # Can do as whole separate character file names (easiest), or different form.

}







