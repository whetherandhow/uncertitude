
# MO_funs -- miscellaneous functions to support raster-based multi-objective
# optimization under uncertainty


# Contains the following functions, slowly being converted to roxygen doc format

# adj_py_model_script
# build_order
# call_model
# compare_soln_to_frontier
# create_allowability_rasters
# delete_files
# fit_frontier
# obj_scores_from_soln
# op_sample
# op_sample_old
# project_to_front
# rasterize_restrictions
# rasters_to_matrix
# soln_to_map
# stopifnot_wm


##******************************************************************************
##
#### adj_py_model_script ####
##
##******************************************************************************

#' Read in and modify specific arguments from a python script that calls an
#' InVEST model.

#' @param template_script Fully functional script to call an InVEST model from
#'   console, see details.
#'
#' @param args_to_set Two-column data frame that maps model argument names to
#'   column headers in the experimental design file, see details.
#'
#' @param outfile Character string specifying file name for modified script.
#'
#' @details args_to_set is two columns, col 1 specifies argument name as
#'   recognized by ES model, col 2 specifies name in exp design table. Besides
#'   convenience in the event a user finds a different name more intuitive, this
#'   also allows one to have a single experimental design that passes different
#'   values of a common input to different models, which may be useful for
#'   sensitiviity analysis.
#'
#' @export

adj_py_model_script <- function(template_script, args_to_set, outfile){

  script_lines <- readLines(template_script)

  #Find the line with the argument:
  for (i in 1:nrow(args_to_set)){

    current_arg <- args_to_set[i, "to_set"] # eg, "lulc_raster_path"
    arg_ind <- grep(paste0(current_arg,"':"), script_lines)
    script_lines[arg_ind] <- paste0("        u'", args_to_set[i, "to_set"],
                                   "': u'", args_to_set[i, "set_to"], "',")

  }

  fileConn <- file(outfile)
  writeLines(script_lines, fileConn)
  close(fileConn)

}


##******************************************************************************
##
#### build_order ####
##
##******************************************************************************

#' construct a partial ordering from a table. PARTIAL DOCS, NOT ROBUST
#'
#' @param table_to_perturb NEED STUFF HERE.
#' @param inc_cols NEED STUFF HERE.
#' @param eq_tols Scalar or vector of length equal to number of classes in
#'   table, representing an equality tolerance so that params within that
#'   (relative) distance don't require the order be preserved.
#'
#' @export


build_order <- function(table_to_perturb, inc_cols, eq_tols=.05){

  # Build a (partial) ordering defined by unambiguously lower sets and upper sets
  # eq_tols is an equality tolerance so that params within that distance doesn't
  # require the order be preserved.

  # NICETY: specify eq_tols as absolute not just relative

  n_classes <- nrow(table_to_perturb)

  if(length(eq_tols)==1) {
    eq_tols <- rep(eq_tols, n_classes)
  } else if (length(eq_tols)!=n_classes) {
    stop("eq_tols must be either length 1 or length = nrow(table_to_perturb)")
  }

  order_list <- list()

  for (i in inc_cols){

    order_list[[i]] <- list()

    for (j in 1:n_classes){

      order_list[[i]][[j]] <- list()

      lower_set <- which(table_to_perturb[, i] <= (table_to_perturb[j, i]*(1 - eq_tols[j])))
      upper_set <- which(table_to_perturb[, i] >= (table_to_perturb[j, i]*(1 + eq_tols[j])))

      order_list[[i]][[j]]$lower_set <- lower_set
      order_list[[i]][[j]]$upper_set <- upper_set

    }

  }

  return(order_list)

}




##******************************************************************************
##
#### compare_soln_to_frontier ####  NOT DONE NOT DONE
##
##******************************************************************************

compare_soln_to_frontier <- function(soln, frontier, mv_list, method="impute_frontier"){

#' Find performance of a solution relative to a frontier -- INCOMPLETE

#' @param frontier list of [something] that are used either to evaluate against, or
#'             or as frontier. If a frontier, can be specified as just perf
#'             metrics under different weight combos. If doing as testing to
#'             explicit other solutions, then SOMETHING else.

#' @param soln solution, specified as an a spatial data frame containing an
#'            integer-coded column named portfolio, where integers correspond to
#'             indices in the marginal value tables
#'
#' @export


#  method :  summarize by reference to solutions on the frontier, or to a
# projected combination of
## objective scores ie, if we had run the optimizer under that combo, where
# would it be? This should be reasonable for decision problems where the
# frontier can in theory be filled in with arbitrary density. (Ie, those with
# extremely high numbers of decision units (like pixels), and/or continues
# variables.)


##  Value  :  A ... vector of length n_obj + 1, where n_obj is the number of
#             objectives being evaluated. The first entry is the relative
#             performance, and the others are the distance to the frontier on
#             that objective.


# First step is to evaluate the solution in the state of the world defined by
# by mv_list

  obj_scores <- obj_scores_from_soln(soln, mv_list)

## Second step is to summarize those relative to the frontier

  if (method=="impute_frontier"){

    front <- fit_frontier(obj, frontier)  #a function that returns y value of frontier
    #given x

    imputed_best <- project_to_front(obj_scores, front)
    rel_dist_to_frontier <- dist(rbind(obj_scores, imputed_best))/
      dist(rbind(c(0,0), imputed_best))

    summary_score <- rel_dist_to_frontier

    #multi-dimensional, should work for any number of objectives:
    #(also... default in absolute terms)
    regret_for_weighting <- imputed_best - obj_scores

    return(c(summary_score, regret_for_weighting))

    #TODO: add implied weighting combo?

  } else if (method=="other_solns"){

    stop("Sorry, assessment relative to other solutions is not yet implemented...")

  } else {

    stop(
      "Method argument unrecognized. Currently must be either `impute frontier' or 'other_solns'"
    )

  }

}




##******************************************************************************
##
#### create_allowability_rasters ####
##
##******************************************************************************

# Supersedes 'rasterize_restrictions'

#' Rasterize restriction and allowability masks
#'
#' @param layers A specially formatted dataframe or csv listing the polygons for
#'   prefer and and prevent areas -- see details.
#'
#' @param template_raster A raster aligned with all other rasters in the
#'   (typically a base land cover map), with NAs outside the area of interest.
#'
#' @param suffix Character and raster file extention that will be appended to
#'   the intervention name when writing out the final allowability raster for
#'   each intervention.
#'
#' @param interv_list Optional -- if not NA, then an appropriately formatted
#'   list describing intervention names and their eligible land classes, as
#'   used throughout the broader workflow (TODO SEE OTHER DOCUMENTATION).
#'
#' @return RasterStack, also writes out files
#'
#' @details Certain spatial interventions can only be performed on certain LULC
#'   types or are otherwise restricted by geography or administrative units. Eg,
#'   riparian buffer strips need to be next to streams, actions aimed at private
#'   landowners can't be undertaken on government land. To enforce these
#'   constraints in an optimization, a solver needs to have pixel-level
#'   information about where each activity is allowed. This function transforms
#'   polygon based information about where interventions are not allowed into
#'   a raster, and also aggregates multiple rasters to create a single layer
#'   for each intervention, describing where it is allowed. The main input is
#'   a specially formatted csv with five columns: "intervention" (which
#'   requires an exact string match), "dir" and "file" which specify the input,
#'   and "restriction" which needs to be a character string of "prohibit_on" or "restrict_to".
#'   Files can be raster or polygons, but (currently) needs to specified in a
#'   separate column "spatial_type" with allowable fields "polygon" and "raster"
#'    -- polygons will first be rasterized with raster::rasterize.
#'
#'   NOTE/TODO handling of polygon dbf associated values -- they are ignored?
#'   NOTE/TODO Assumptions on template raster, LULC, values outside watershed...
#'   NOTE/TODO Shapefiles should be provided without .shp, but rasters should
#'   have their extention (calling rgdal::readOGR and raster::raster,
#'   respectively). More broadly, let them be provided with extension and
#'   remove requirement to specify type.
#'   NOTE/TODO usage of suffix.
#'
#' @export


create_allowability_rasters <- function(layers, template_raster, suffix, interv_list=NA){

  # Check format:
  if(is.character(layers)){
    if(file_ext(layers)=="csv"){
      layers <- read.csv(layers, stringsAsFactors = FALSE)
    } else {
      stop("Unrecognized character format for 'layers' argument.")
    }
  } else if (!is.data.frame(layers)){
    stop("Argument 'layers' not of appropriate type. See help for details.")
  }

  # Check data frame has necessary columns:
  have_all <-  c("intervention", "dir", "file",
                 "spatial_type", "restriction") %in% names(layers)
  stopifnot_wm(all(have_all),
               "Argument 'layers' missing appropriately named columns.
               See help file.")

  # Check restriction column has right values:
  stopifnot_wm(all(layers$restriction%in%c("restrict_to", "prohibit_on")),
               "Unrecognized terms used in 'restriction' column of 'layers'.")

  # First, find each unique set of interventions:
  intervs <- unique(layers$intervention)

  for (i in 1:length(intervs)){

    active_interv_name <- intervs[i]

    #Extract which indices are relevant to that intervention:
    active_interv_restrictions <- which(layers$intervention==intervs[i])

    # Reset for each intervention set:
    allowed_working <- makeTRUEorNA(!is.na(template_raster))

    for (j in active_interv_restrictions){


      file_dir <- layers[j, "dir"]
      layer_name <- layers[j, "file"]

      # Polygon or raster?
      if(layers$spatial_type[j]=="polygon"){ #Rasterize

        restrict_layer <- readOGR(dsn = file_dir, layer = layer_name)

        plot(template_raster, main=j)
        plot(restrict_layer, add=TRUE)

        cat(date(), ": Rasterizing row ", j, "of restriction layer data frame...\n")
        print(layers[j, c("intervention", "file", "restriction")])
        mask_working <- rasterize(restrict_layer, template_raster, update=FALSE, progress="text")

#        mask_working <- rasterize(restrict_layer, template_raster, field="gridcode", update=FALSE, progress="text")

        # Deal with allow or prohibit by converting to make "true" allowable:

        if(layers$restriction[j]=="prohibit_on"){
          allowed_current <- makeTRUEorNA(is.na(mask_working))
        } else if(layers$restriction[j]=="restrict_to"){
          allowed_current <- makeTRUEorNA(!is.na(mask_working))
        }

      } else if (layers$spatial_type[j]=="raster"){

        mask_working <- raster(paste0(layers[j, "dir"], "/", layers[j, "file"]))

        #browser()
        if(layers$restriction[j]=="prohibit_on"){

          # Account for varying possibilities for the non-true pixels being
          # NA or FALSE, to end up with only TRUE's for allowable, or NA's:
          allowed_current <-makeTRUEorNA(!mask_working & !is.na(template_raster))

        } else if(layers$restriction[j]=="restrict_to"){

          # Accept wherever is TRUE (FALSE or NA disqualifies)
          allowed_current <- makeTRUEorNA(mask_working==TRUE)

        }

      } else {
        stop("Unrecognized value for spatial_type entry in layers
             -- should be 'raster' or 'polygon'")
      }

    # browser()
    # Update the working raster of allowed places for the current interv:
    allowed_working <- makeTRUEorNA(allowed_working & allowed_current)

    # plot(allowed_working)

    } # End loop through each restriction layer

    # Finally, enforce possibly LULC-based constraints:
    if(!is.logical(interv_list)){

      lulc_restricts <-
        template_raster%in%interv_list[[active_interv_name]]$eligible_classes
      lulc_restricts[lulc_restricts!=TRUE] <- NA
      allowed_working <- allowed_working & lulc_restricts

    }

    stopifnot_wm(all((allowed_working%in%c(NA, TRUE))[]),
                 "FALSEs exist in allowability layer -- expecting TRUE or NAs.")

    writeRaster(allowed_working,
                file=paste(active_interv_name, suffix, sep="_"), overwrite=TRUE)

    return(allowed_working)

  } # End loop through each intervention

}

##******************************************************************************
##
####  delete_files  ####
##
##*****************************************************************************

#' Delete extraneous output files from batched models.

#' @param files_csv (Character) Path to a specially formatted csv file
#'   indicating what outputs can be deleted.
#' @param suffix Character specifying "results suffix" between root and file
#'   extension.
#' @return NULL - acts on file system.
#'
#' @details 'files_csv' expects at least two columns: one ('path_and_stem')
#'   indicating a path and
#'   file name stem RELATIVE to the model workspace directory, and another
#'   ('extention') indicating the extension (including period). It will then
#'   insert the suffix in between. For convenience it may also include a
#'   "model column" which is ignored.
#'
#' @export

delete_files <- function(files_csv, suffix, model_workspace) {

  files_table <- read.csv(files_csv)
  files_table <- files_table[files_table$remove==1, ] #remove only flagged files

  files_to_delete <-
    paste0(model_workspace, "/", files_table$path_and_stem, "_", suffix, files_table$extension)

  file.remove(files_to_delete)

}


##******************************************************************************
##
#### fit_frontier ####
##
##******************************************************************************

fit_frontier <- function(obj, xmax_scalar=1.5){

#' Fit a continuous frontier function to a set of points -- NOT COMPLETE
#' @export
# Arguments:

#    obj : n by 2 data frame or matrix with objecive for x axis in first column
#          and objective for y axis in second column

# xmax_scalar : used later in searching for uniroot

#  Value : a function that returns the predicted y value of the frontier
#          when given an x value

  #NOTE, in case it got lost somewhere, cobs is in a library of same name

  frontier <- cobs(x=obj[,1], y=obj[,2], constraint="concave")

  frontier_fun <- function(x){predict(frontier, z=x)[1, 2]} #predict.cobs method

  #Needs to return maximum value of x for use in search by uniroot.
  attr(frontier_fun, "max_x") <- max(obj[ ,1])*xmax_scalar

  #NOTE/TODO: formulation may assume upper right quadrant...

  return(frontier_fun)

}




#### makeTRUEorNA ####

# The raster package has various functions that are more easily used when your
# raster is TRUE or NA, so found myself doing this a lot:

makeTRUEorNA <- function(x){
  x[x!=TRUE] <- NA
  return(x)
}


##******************************************************************************
##
#### obj_scores_from_soln ###
##
##******************************************************************************

obj_scores_from_soln <- function(soln, mv_list){
  #soln = solution as aspatial data frame
  #mv_list = list of marginal value tables, 1 per objective

  #Todo: robustify perhaps with naming of obj_scores

  obj_score_vec <- rep(NA, length(mv_list)+1) #1 extra for overall objective

  for (i in 1:length(mv_list)){

    obj_score_vec[i] <- sum(soln[[i]]$in_port * mv_list[[j]][cbind(1:nrow(mv_list[[j]]), soln[[i]]$portfolio)], na.rm =
                              TRUE)

  }

  return(obj_score_vec)

}


##******************************************************************************
##
####  op_sample  ####
##
##******************************************************************************

#' Order-preserving sampling of tables -- WORKING, FRAGILE, INCOMPLETE DOCS
#' @export

# Right now works by just generating random samples and throwing away the
# ones that don't satisfy ordering constraints. Probably smarter ways...

op_sample <- function(table_to_perturb, inc_cols,
                          lower_scale = .5,
                          upper_scale = 2,
                          eq_tols=.05,
                          max_iter = 100){

  ## INPUTS:

  # table_to_perturb : DataFrame, with columns to randomly perturb

  # inc_cols : which columns of the data frame to perturb (can be index or names).
  #            TODO: Right now only works on numeric variables.

  # lower_scale, upper_scale: single numbers or vectors with
  # length = nrow(table_to_perturb), which specify the multiplier to use to get
  # the lower and upper bound for each parameter, relative to the default value
  # in table_to_perturb.

  # max_iter  : how many random draws to try before giving up on finding on that
  #             satisfies ordering constraints

  # NICETY: could make these absolute bounds too.
  # NICETY: could offer alternate sampling approaches besides runif

  # Initialize:
  new_table <- table_to_perturb
  new_table[, inc_cols] <- NA

  n_classes <- nrow(table_to_perturb)

  p_order <- build_order(table_to_perturb, inc_cols, eq_tols=.05)
  #NICETY: option to pass or build p_order

  for (i in inc_cols){

    cat("Starting column ", i, " of ", length(inc_cols), " total. \n")

    param_ranges <- data.frame(center=table_to_perturb[, i])
    param_ranges$l_lim <- lower_scale * param_ranges$center
    param_ranges$u_lim <- upper_scale * param_ranges$center

    ref_order <- order(param_ranges$center)

    #Generate sample:
    accept <- FALSE
    iter <- 1

    while(!accept & (iter <= max_iter)){

      unif_starts <- runif(nrow(param_ranges))
      param_samp  <- with(param_ranges, l_lim + unif_starts*(u_lim - l_lim))

      # Now test whether satsifies orderings:
      # OLD WAY, too restrictive:
      #if (identical(order(param_samp), ref_order)) accept <- TRUE

      # New tedious way: test sets for each variable.
      classes_ok <- TRUE
      class_iter <- 1

      while (classes_ok & (class_iter <= n_classes)) {

        #Iterate through classes, stop when one fails
        classes_ok <- FALSE

        active_lower <- which(table_to_perturb[, i] <= (table_to_perturb[class_iter, i]))
        active_upper <- which(table_to_perturb[, i] >= (table_to_perturb[class_iter, i]))

        stored_lower <- p_order[[i]][[class_iter]]$lower_set
        stored_upper <- p_order[[i]][[class_iter]]$upper_set

        # Is everything that's supposed to be below below, and everything that's
        # supposed to be above above?

        # 3 cases:

        # Do for lower first:

        if ((length(active_lower) > 0) & (length(stored_lower) > 0)){
        # 1: Both sets non-empty:

          # NB: Which is on which side of %in% matters!
          # The test is that everything that's supposed to be in the set
          # is there, it's ok if other things are there too
          pass_lower <- all(stored_lower %in% active_lower)

        } else if (length(stored_lower)==0){
        # 2: stored set is empty -- is therefore member of active set
            pass_lower <- TRUE
        } else if (length(stored_lower) > 0){
        # 3: stored is non-empty, but active set is empty.
          #NB: If structure --> length(active_lower)==0
          pass_lower <- FALSE #should be something in set that's not there
        }

        # Then upper:

        if ((length(active_upper) > 0) & (length(stored_upper) > 0)){
          # 1: Both sets non-empty:

          # NB: Which is on which side of %in% matters!
          # The test is that everything that's supposed to be in the set
          # is there, it's ok if other things are there too
          pass_upper <- all(stored_upper %in% active_upper)

        } else if (length(stored_upper)==0){
          # 2: stored set is empty -- is therefore member of active set
          pass_upper <- TRUE
        } else if (length(stored_upper) > 0){
          # 3: stored is non-empty, but active set is empty.
          #NB: If structure --> length(active_upper)==0
          pass_upper <- FALSE #should be something in set that's not there
        }

        classes_ok <- pass_lower & pass_upper
        class_iter <- class_iter+1

      }

      #If make it through all classes, then accept:
      if (classes_ok) accept <- TRUE

      iter <- iter + 1

    }

    if (accept){
      new_table[, i] <- param_samp
    } else {
      new_table[, i] <- table_to_perturb[, i]
      warning(paste("Did not find acceptable ordering for column ", i, " after ", iter, " attempts.
                    Assigning original table value for column ", i, ".", sep=""))
    }

    }

  return(new_table)

}




##******************************************************************************
##
#### project_to_front ####
##
##******************************************************************************

project_to_front <- function(obj_scores, front){

#' Helper function -- identify where on a frontier a sub-optimal solution would
#' be under an imputed weight combination. FRAGILE, DOCUMENTATION INCOMPLETE.
#' @export

  # ASSUMES 2 objective for now!

  #"radially" project a strategy to the imputed pareto frontier to find the
  # objected scores associated with a (to-be-discovered) portfolio, optimal
  # in that state of the world for that weighting combination

  # probably a higher-math way to do this, but for now, basically rootfind:

  slope <- obj_scores[2]/obj_scores[1]

  xval <- uniroot(f = function(x){front(x) - slope*x},
                  interval = c(0, attr(front, "max_x")))$root

  yval <- front(xval)

  return(c(xval, yval))

}




##******************************************************************************
##
#### rasters_to_matrix NOT COMPLETE ####
##
##******************************************************************************

#' Convert rasters into aspatial vectors that then become columns of a matrix
#' Initially made for converting pixel based constraints on interventions into
#' the form desired by the greedy solver, but probably useful for other things!

rasters_to_matrix <- function(x){

  nrow_mat <- N #npix
  ncol_mat <- nlayers # in stack

  aspat_mat <- matrix(NA, nrow=nrow_mat, ncol=ncol_mat)

  for (i in 1:nlayers){

    aspat_mat[ , i] <- getValues(x[LAYER_i])

  }

  colnames(x) <- LAYER_NAMES

}




##******************************************************************************
##
#### soln_to_map ####
##
##******************************************************************************

#' Convert vector-based solution to raster -- DOCs INCOMPLETE
#' @param soln single solution as vector (not raster), also,
#'             not list of vectors corresponding to multiple weightings
#' @param soln_only Logical. Return NA's where activities are not specifically
#'   chosen (otherwise overlay on lulc_base).
#' @export

soln_to_map <- function(soln, lulc_base, interv_bpt_map=NA, soln_only=FALSE,
                        donothing_code=1, check_conform=TRUE){

  # soln is
  # soln_only (for now) codes the interventions according to their integer
  # number, not land use class.

  # weak test to conformability:

  if(check_conform){
    stopifnot(length(lulc_base)==nrow(soln))
  }

  #OOps - more complicated than this (if doing this approach, need to add
  #stuff to extract the values themselves
  #  if(soln_only){
  #    if(!is.logical(interv_bpt_map)){
  #      interv_bpt_map <- NA
  #      print("Argument interv_bpt_map included but ignored
  #            (because soln_only = TRUE")
  #    }
  #  }


  soln_rast <- lulc_base  #Changes will be overlayed on top, one intervention at
  #a time.

  if(soln_only){
    soln_rast[] <- NA
    #    if(!is.na(interv_bpt_map)){
    code_arg <- "interv_num"
    #    }
  } else {
    code_arg <- "bpt_code"
    #TODO: add check that bpt map is not NA at same time soln_only is FALSE
    #(it will break anyway, but just more informative for user)
  }

  for (i in 1:nrow(interv_bpt_map)){

    #Make temprast a binary raster, once for each solution class:
    temp_rast <- lulc_base #Just for use as a template
    temp_rast <- setValues(temp_rast, soln$portfolio)
    temp_rast[temp_rast==donothing_code] <- NA
    soln_rast <- update_lulc(lulc_base=soln_rast,
                             changed_map=(temp_rast==interv_bpt_map[i, "interv_num"]),
                             newval_code=interv_bpt_map[i, code_arg])

    #NOTE, this could be all implemented in a different (faster?) form, by reclassifying
    #the soln and then doing direct overlay. This approach used for now becuase
    #of a few robustness considerations in update_lulc

  }

  return(soln_rast)

}




#### stopifnot_wm ####

# Super simple wrapper function to reduce if statements. Behaveslike stopifnot,
# but lets you output a message.

stopifnot_wm <- function(condition, message){

  if(!condition){
    stop(message)
  }

}
