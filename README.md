# README #

### What is this repository for? ###

* The ultimate goal of this package and related code is to make it easier to do:
    * **Uncertainty and sensitivity analysis** of raster-based ecosystem service models
    * **Spatial optimization** (*where* to do *what* on a landscape to achieve multiple objectives)
    * **Map comparison** of spatial scenarios and their ecosystem service performance

It is assumed that achieving the above requires the ability to:

* Specify many sets of model runs
* Batch run the ecosystem services models
* Read and summarize their output spatially and aspatially

The repository therefore contains functions to assist with the above tasks (these are well-documented R functions on their way to becoming a package), and contains a number of scripts in the main directory that illustrate their use. It is motivated by analysis conducted at the Natural Capital Project, it can be considered a start at an R-based version of "RIOS 2.0" that allows for targeting watershed interventions based on actual estimates of their marginal values. Python-based code with similar functionality is also available, from Richard Sharp and Peter Hawthorne. 

#### Vignettes

Three vignettes are currently in the works: (please contact if some would be of immediate use)

* Basics of batch-running InVEST using R as the wrapper to setup experiments and call the InVEST models.
* Handy functions for reading and analyzing large numbers of rasters (categorical and continuous), as might be produced in uncertainty analysis or optimization.
* Putting it all together with a fairly complex workflow for spatial marginal-value based optimization with many scenarios.


#### Version - 0.0.1-9000  (not incrementing yet, until minimum package build, but you can of course track the commits).


### Quick guide to code structure

Generally, there is a script for each of several different major steps of:

* Creating a set of computational experiments
* Greedy simple optimization
* Reading in, comparing and summarizing lots of rasters for sensitivity analysis and optimization.

At the moment, things are still quite preliminary, so here's just a little info:

In general, files ending with "_funs.R" are actually mostly legit, clean and somewhat reasonably documented -- current work is formally documenting files in the R folder with roxygen notation for clean package build. Files with "scripting" or "processing" in their name are somewhat structured, but not guaranteed to work start to bottom yet. Anything else (especially things with "misc" in the title) is just a pile of random code waiting to get sorted through. 

MO_documentation.Rmd is for now an R markdown file (from which you can make html, word or pdf files), where I'm attempting to consolidate the practical "how-to" side of things. Thus far it is not anywhere close to something more like detailed package documentation.

### Files of definite use and in decently good shape

core_optim_workflow.R -- assuming existence of all inputs and experimental design table, script to generate model runs, difference to get marginal value tables, and feed those marginal value tables + costs to greedy solver to get portfolios. Generally solide, but as of 2017-01-04 (Commit #2624278), requires heavy case-specific editing. NOTE this is getting parted out into smaller scripts, but they are still called from core_optim_workflow, so you can find their names there until things are more solidified.

greedy.R -- reasonably fast and not-memory limited greedy solver -- but very functionally limited in terms of handling constraints and that sort of thing.

sampling_design_generation.R -- generally good shape chunks of wrapper code to build different experimental designs (latin hypercube, one at a time, full factorial). Probably duplicates some functionality that exists in other real packages, but useful. 

summary_funs.R contains various ways of creating summary statistics on (I believe) pixel-level data.

### Files likely to be of use but requiring inspection to use or figure out why

Listed in approximate order of expected utility:

optim_diagnostic_plots.R -- make various scatter plots and others to just check marginal values for reasonableness. Runs reasonably reliability if inputs are there, but needs much more "functionification" before being generalizable.

misc_optim_output_scripting.R -- variety of code snippets to makes somewhat aesthetically pleasing tradeoff plots and maps, used I believe mainly for ESA 2015 figures -- probably significantly but not entirely superseded.

scripting_for_RIOS_sensitivity_guidance_doc.R -- Code designed to produce workflow, figures and summaries required that went into Bryant, Wolny and Vogl 2015 doc testing RIOS sensitivities. 

spatial_dependency_assessment.R -- start at scripting to make maps of varying levels of pixel "flipping" and testing impact on marginal values and aggregate ES outcomes.
  
read_in_and_make_variance_map -- code to script reading in a bunch of maps into a raster stack and making some plots based on pixel-level statistics. Mostly remnant of 2015 SC3/RIOS sensitivity analysis, but maybe still useful. 

one_way_sensitivity_processing.R -- likely obsolete or better done with formal sensitivity analysis approaches, but probably contains some useful tricks and reminders of structure.

importing_and_aligning_funs.R -- contains tifs_to_RasterStack and also a function for adding raster attribute tables. Not sure how useful it is in new MO setup vs mainly for RIOS. But useful to remember exists in case want to draw on.

### Files not sure if superseded or if contain relevant info

first_draft_exp_design_or_WLE.R -- Not sure if this got superseded or is still a tedious way of building up a factorial design for optim.

misc_remnants (As of 2017-01-04) contains remnants of:

* choose_activities.R -- PRETTY sure this is old RIOS stuff superseded by greedy and other bits of code, but not sure.

* frontier_fitting.R -- contains starts at testing fitting of frontier curves with spline and other things. Promising, but not functionified.

* misc_plotting.R -- A couple functions for maybe plotting one way sensitivities and fiddling with hillshades and stuff, that may be old and already built into plotting_funs.R by now.