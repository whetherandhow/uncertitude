#### Ultimately this should become functions that get incorporated into other
## function-storing files, but for now trying to set up vaguely generic clean
## workflow...


library(raster)
library(plyr)
library(ggplot2)
library(reshape2)
#library(rjson) -- I think only necc in "make_static_..."
#library(cobs) #for 2-d frontier fitting [done elsewhere?]

source("C:/Users/bpb/Dropbox/uncertitude/MOfuns.r")
source("C:/USers/bpb/Box Sync/uniluc/rillest/rillest_functions.r")
source("C:/Users/bpb/Dropbox/uncertitude/plotting_funs.r")
source("C:/Users/bpb/Dropbox/uncertitude/summary_funs.r")

setwd("C:/Users/bpb/Dropbox/Tana Optim Set")

## Specify an experimental design set:

## Load in the marginal value outputs:
mvti <- 3
expi <- 2
cost_combo <- 3

# Base land use for use in plots later:
base_lulc <- raster("C:/Users/bpb/Dropbox/Optimizat_Data-iferous/SDR_wateryield_data_10.27.15/lulc_32.tif")
load("inter_ext.rda")
lulc_cropped <- crop(base_lulc, inter_ext)



#load(paste("soln_", mvti, "_", expi, "_", cost_combo, ".rda", sep = ""))
load(paste0("marg_val_byobj_", mvti, "_", expi, ".rda"))

inames <- colnames(marg_val_byobj[[1]])
#load (STUFF)

## Need to do a bunch of processing to get a data frame that has rows for each
## combination of pixel, objective and intervention

mv1 <- as.data.frame(marg_val_byobj[[1]])
mv2 <- as.data.frame(marg_val_byobj[[2]])

#if invert: (to make sed reduction goood)
mv1 <- -1*mv1

n_interv <- ncol(mv1)
npix <- nrow(marg_val_byobj[[1]])

#if(is.null(names(mv1))) names(mv1) <- inames
#if(is.null(names(mv2))) names(mv2) <- inames

#names(mv1) <- letters[1:ncol(mv1)]
#names(mv2) <- letters[1:ncol(mv2)]

mv1_melt <- melt(mv1)
mv2_melt <- melt(mv2)

#TODO: Store naming structure in model itself
mv1_melt$obj <- "sed"
mv2_melt$obj <- "awy"

names(mv1_melt) <- c("interv", "value", "obj")
names(mv2_melt) <- c("interv", "value", "obj")

#Why not work?
#mvm <- merge(mv1_melt, mv2_melt, by="interv")

# Check same order of interv:
stopifnot(identical(mv1_melt$interv, mv2_melt$interv))
#TODO -- also add pixel level check or do formal merge

#mvs <- rbind(mv1_melt, mv2_melt)

#mvw <- dcast(mvs, interv + obj ~ value)
#Seems needlessly slow and memory hogging... for now do manually?
#intervs <- unique(mvw$interv)
#objs <- unique(mvw$obj)
#mv_med <- data.frame(interv=rep(NA, npix*n_interv), sed=NA, awy=NA)
mv_med <- cbind(mv1_melt[ , c("interv", "value")], awy_obj=mv2_melt$value)
names(mv_med) <- c("interv", "sed_obj", "awy_obj")

unique_lulcs <- unique(lulc_cropped)
bpt <- read.csv("C:\\Users\\bpb\\Dropbox\\Tana optim set\\SWY\\inputset_2016-09-ish\\biophysical_table_s_07062016.csv")
#lulc_base_vec <- factor(getValues(lulc_cropped), levels=unique_lulcs, labels=letters[1:length(unique_lulcs)])
lulc_base_vec <- factor(getValues(lulc_cropped), levels=unique_lulcs, labels=bpt$description[unique_lulcs])

mv_med <- cbind(mv_med, lu_from=lulc_base_vec)

mv_med <- mv_med[complete.cases(mv_med), ] #Slow?, could do manually...
mv_nobase <- mv_med[mv_med$interv!="base", ]
mv_nobase$interv <- droplevels(mv_nobase$interv) #Make legends work right

#### Basic plot faceted by intervention, ALL POINTS ####

plot_samp_size <- 10000
plot_samp <- sample(nrow(mv_nobase), n_interv*plot_samp_size)

# All marginal values, unsorted
ggplot(mv_nobase[plot_samp, ], aes(x=sed_obj, y=awy_obj)) + geom_point(alpha=I(.05)) +
  #facet_wrap("interv") +
  geom_hline(yintercept = 0, color="red") +
  geom_vline(xintercept=0, color="red") + theme_bw()


# Colored by intervention
ggplot(mv_nobase[plot_samp, ], aes(x=sed_obj, y=awy_obj)) + geom_point(aes(color=interv), shape=16, alpha=I(.1)) +
  #facet_wrap("interv") +
  geom_hline(yintercept = 0, color="red") +
  geom_vline(xintercept=0, color="red") + theme_bw() +
  guides(colour = guide_legend(override.aes = list(alpha = 1), title="Intervention")) +
  xlim(-25, 50)

# Faceted by intervention
ggplot(mv_nobase[plot_samp, ], aes(x=sed_obj, y=awy_obj)) + geom_point(alpha=I(.05)) +
  facet_wrap("interv") +
  geom_hline(yintercept = 0, color="red") +
  geom_vline(xintercept=0, color="red") + theme_bw() +
  xlim(-25, 50)

#### Enhance by color-coding according to land use ####
lu_plot_samp <- sample(nrow(mv_nobase), n_interv*plot_samp_size)
ggplot( mv_nobase[lu_plot_samp, ], aes(x=sed_obj, y=awy_obj,
                                       group=interv, color=lu_from)) +
  guides(colour = guide_legend(override.aes = list(alpha = 1), title="LULC")) +
  geom_point(alpha=.1) + facet_wrap("interv") + theme_bw() +
  geom_hline(yintercept = 0, color="red") +
  geom_vline(xintercept=0, color="red") + theme_bw() +
  xlim(-25, 50) + xlab("Change in Sediment export (tons/yr)") +
  ylab("Change in Water Yield (mm/yr)")

#### Assess whether there is tradeoff between distributional objectives ####

# soil conservation by crop type #


lulc_base <- raster("C:\\Users\\bpb\\Dropbox\\Tana optim set\\lulc_base.tif")
lulc_terracing <- raster("C:\\Users\\bpb\\Dropbox\\Tana optim set\\lulc_terracing.tif")

levelplot(lulc_base)
